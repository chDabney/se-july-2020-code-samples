import multer from "multer";
import path from "path"; //no need to npm install this, it is built in
import { v4 } from "uuid";
// TODO fix me this is bad
const uploadDirectory = "./uploads";

// this is our diskStorage for multer
const diskStorage = multer.diskStorage({
  // where we want to store the files
  destination: (req, file, callback) => {
    callback(null, uploadDirectory);
  },
  // how we should name the files when we save them
  filename: (req, file, callback) => {
    const fileExtension = path.extname(file.originalname);
    const filename = `${v4()}${fileExtension}`;
    callback(null, filename);
  },
});

export const diskUploader = multer({ storage: diskStorage });
