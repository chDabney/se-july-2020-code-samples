import express from "express";
import usersJSONFile from "./data/users.json";
import { diskUploader } from "./utils";
// Allows us to make changes and access this usersJSON variable
// instead of reassigning a new variable each time we made changes
let usersJSON = usersJSONFile;

// npm init esm <-- generates this cool scaffolding
// npm install express - install express to run your app
// npm instal -D nodemon - install nodemon to restart server on file changes
// start script - package.json - "node index.js"
// dev script - package.json - "nodemon index.js"

/*
GET - retreive data
POST - create an entity
PUT - replace an entity and all of its data
PATCH - update an entity with some or all of its data
DELETE - deletes an entity
*/
const app = express();
// we want to parse incoming JSON data if we have it
app.use(express.json());
// we want to parse information in our url
app.use(express.urlencoded({ extended: true }));

app.use((req, res, next) => {
  console.log("Hello from the cool middleware");
  next();
});

app.use((req, res, next) => {
  console.log("Hello from the cool middleware 2");
  req.whatever = usersJSON[0];
  req.sayMyName = () => {
    console.log(usersJSON[0].name);
  };
  next();
});

app.get("/me", (req, res) => {
  req.sayMyName();
  res.send(req.whatever);
});

app.post("/upload", diskUploader.single("filez_bruh"), (req, res, next) => {
  res.send("Thanks for uploading the file!");
});

/*
req - all of the incoming information (the request) coming into our server
res - sending data back to the client that requested the information (sending a response)
*/
app.get("/", (req, res) => {
  res.send("Hello from my dope server!");
});

app.get("/users", (req, res) => {
  // res.json(usersJSON);
  // res.send(usersJSON);
  // sending JSON with a 200 status code
  // always call .status(Number) before send or json
  res.status(200).json(usersJSON);
});

// Additional GET functionality for getting a specific entity
app.get("/users/:id", (req, res) => {
  // .find() accepts a callback function that should return a true or false statement.
  // Finds the first instance of the result being true and returns that value
  const user = usersJSON.find((user) => user.id === Number(req.params.id));

  // Fail check to see if requested data doesn't exist.
  // Should respond with a 404 and "return" to exit the function.
  if (!user) {
    res.status(404).send("User ID not found.");
    return;
  }

  // If requested data exists send a 200 status with the requested user.
  res.status(200).json(user);
});

app.post("/users", (req, res) => {
  // req.body is information from the requests body and you have access
  // to it (POST/PUT/PATCH - requests)

  // TODO: FIX ME!!
  const person = req.body;
  usersJSON.push(person);
  // 201 status code for creating things
  res.status(201).send("ok!");
});

// PUT - replace an entity and all of its data
app.put("/users/:id", (req, res) => {
  // .findIndex() accepts a callback function that should return a true or false statement.
  // Finds the first instance of the result being true and returns the INDEX of that value.
  const userIndex = usersJSON.findIndex((user) => user.id === Number(req.params.id));

  // Fail check to see if requested data doesn't exist.
  // Should respond with a 404 and "return" to exit the function.
  if (userIndex === -1) {
    res.status(404).send("User not found.");
    return;
  }

  // Array bracket notation to access a specific index.
  // Reassigns the value at the userIndex to this object.
  usersJSON[userIndex] = {
    ...req.body,
    id: Number(req.params.id),
  };

  // If requested data exists send a 200 status with a success message.
  res.status(200).send("Updated successfully!");
});

// PATCH - update an entity with some or all of its data
app.patch("/users/:id", (req, res) => {
  const userIndex = usersJSON.findIndex((user) => user.id === Number(req.params.id));
  if (userIndex === -1) {
    res.status(404).send("User not found.");
    return;
  }

  // Because we are still spreading usersJSON[userIndex] this functionality represents UPDATING (adding to) usersJSON[userIndex]
  // with any fields specified in the req.body
  usersJSON[userIndex] = {
    ...usersJSON[userIndex],
    ...req.body,
    id: Number(req.params.id),
  };

  res.status(200).send("Updated successfully!");
});

// DELETE - deletes an entity
app.delete("/users/:id", (req, res) => {
  // Reassigning the usersJSON variable so any following GET request
  // will reflect these changes. See line 4 for why we are able to reassign.
  usersJSON = usersJSON.filter((user) => user.id !== Number(req.params.id));

  res.status(200).send("Updated successfully!");
});

// Should always be last because it matches EVERYTHING
// wildcard (*) matches EVERYTHING
app.get("*", (req, res) => {
  res.status(404).json({
    message: "You lost bruh??",
  });
});

// listen to the server on port 4000
app.listen(4000, () => {
  console.log("Express server is now running on port 4000");
});
